package mvc.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "create_answer", schema = "MEDLEY_BD_OSS3", catalog = "")
@IdClass(CreateAnswerEntityPK.class)
public class CreateAnswerEntity {
    private byte answer;
    private byte createP;
    private int idUser;
    private int idQuestion;

    @Basic
    @Column(name = "answer")
    public byte getAnswer() {
        return answer;
    }

    public void setAnswer(byte answer) {
        this.answer = answer;
    }

    @Basic
    @Column(name = "create_p")
    public byte getCreateP() {
        return createP;
    }

    public void setCreateP(byte createP) {
        this.createP = createP;
    }

    @Id
    @Column(name = "id_user")
    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Id
    @Column(name = "id_question")
    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CreateAnswerEntity that = (CreateAnswerEntity) o;

        if (answer != that.answer) return false;
        if (createP != that.createP) return false;
        if (idUser != that.idUser) return false;
        if (idQuestion != that.idQuestion) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) answer;
        result = 31 * result + (int) createP;
        result = 31 * result + idUser;
        result = 31 * result + idQuestion;
        return result;
    }
}
