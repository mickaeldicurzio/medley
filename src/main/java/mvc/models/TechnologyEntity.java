package mvc.models;

import javax.persistence.*;

@Entity
@Table(name = "technology", schema = "MEDLEY_BD_OSS3", catalog = "")
public class TechnologyEntity {
    private int idTechnology;
    private String nameTechnology;
    private String descTechnology;
    private String versionTechnology;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_technology")
    public int getIdTechnology() {
        return idTechnology;
    }

    public void setIdTechnology(int idTechnology) {
        this.idTechnology = idTechnology;
    }

    @Basic
    @Column(name = "name_technology")
    public String getNameTechnology() {
        return nameTechnology;
    }

    public void setNameTechnology(String nameTechnology) {
        this.nameTechnology = nameTechnology;
    }

    @Basic
    @Column(name = "desc_technology")
    public String getDescTechnology() {
        return descTechnology;
    }

    public void setDescTechnology(String descTechnology) {
        this.descTechnology = descTechnology;
    }

    @Basic
    @Column(name = "version_technology")
    public String getVersionTechnology() {
        return versionTechnology;
    }

    public void setVersionTechnology(String versionTechnology) {
        this.versionTechnology = versionTechnology;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TechnologyEntity that = (TechnologyEntity) o;

        if (idTechnology != that.idTechnology) return false;
        if (nameTechnology != null ? !nameTechnology.equals(that.nameTechnology) : that.nameTechnology != null)
            return false;
        if (descTechnology != null ? !descTechnology.equals(that.descTechnology) : that.descTechnology != null)
            return false;
        if (versionTechnology != null ? !versionTechnology.equals(that.versionTechnology) : that.versionTechnology != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTechnology;
        result = 31 * result + (nameTechnology != null ? nameTechnology.hashCode() : 0);
        result = 31 * result + (descTechnology != null ? descTechnology.hashCode() : 0);
        result = 31 * result + (versionTechnology != null ? versionTechnology.hashCode() : 0);
        return result;
    }
}
