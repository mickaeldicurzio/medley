package mvc.models;

import javax.persistence.*;

@Entity
@Table(name = "question_technology", schema = "MEDLEY_BD_OSS3", catalog = "")
@IdClass(QuestionTechnologyEntityPK.class)
public class QuestionTechnologyEntity {
    private int idTechnology;
    private int idQuestion;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_technology")
    public int getIdTechnology() {
        return idTechnology;
    }

    public void setIdTechnology(int idTechnology) {
        this.idTechnology = idTechnology;
    }

    @Id
    @Column(name = "id_question")
    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuestionTechnologyEntity that = (QuestionTechnologyEntity) o;

        if (idTechnology != that.idTechnology) return false;
        if (idQuestion != that.idQuestion) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTechnology;
        result = 31 * result + idQuestion;
        return result;
    }
}
