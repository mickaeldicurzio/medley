package mvc.models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class TutorialCategoryEntityPK implements Serializable {
    private int idTutorial;
    private int idCategory;

    @Column(name = "id_tutorial")
    @Id
    public int getIdTutorial() {
        return idTutorial;
    }

    public void setIdTutorial(int idTutorial) {
        this.idTutorial = idTutorial;
    }

    @Column(name = "id_category")
    @Id
    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TutorialCategoryEntityPK that = (TutorialCategoryEntityPK) o;

        if (idTutorial != that.idTutorial) return false;
        if (idCategory != that.idCategory) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTutorial;
        result = 31 * result + idCategory;
        return result;
    }
}
