package mvc.models;

import javax.persistence.*;

@Entity
@Table(name = "tutorial_category", schema = "MEDLEY_BD_OSS3", catalog = "")
@IdClass(TutorialCategoryEntityPK.class)
public class TutorialCategoryEntity {
    private int idTutorial;
    private int idCategory;

    @Id
    @Column(name = "id_tutorial")
    public int getIdTutorial() {
        return idTutorial;
    }

    public void setIdTutorial(int idTutorial) {
        this.idTutorial = idTutorial;
    }

    @Id
    @Column(name = "id_category")
    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TutorialCategoryEntity that = (TutorialCategoryEntity) o;

        if (idTutorial != that.idTutorial) return false;
        if (idCategory != that.idCategory) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTutorial;
        result = 31 * result + idCategory;
        return result;
    }
}
