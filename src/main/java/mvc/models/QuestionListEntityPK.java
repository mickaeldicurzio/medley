package mvc.models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class QuestionListEntityPK implements Serializable {
    private int idList;
    private int idQuestion;

    @Column(name = "id_list")
    @Id
    public int getIdList() {
        return idList;
    }

    public void setIdList(int idList) {
        this.idList = idList;
    }

    @Column(name = "id_question")
    @Id
    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuestionListEntityPK that = (QuestionListEntityPK) o;

        if (idList != that.idList) return false;
        if (idQuestion != that.idQuestion) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idList;
        result = 31 * result + idQuestion;
        return result;
    }
}
