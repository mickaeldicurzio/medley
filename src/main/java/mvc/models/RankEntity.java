package mvc.models;

import javax.persistence.*;

@Entity
@Table(name = "rank", schema = "MEDLEY_BD_OSS3", catalog = "")
public class RankEntity {
    private int idRank;
    private String nameRank;
    private String valueRank;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_rank")
    public int getIdRank() {
        return idRank;
    }

    public void setIdRank(int idRank) {
        this.idRank = idRank;
    }

    @Basic
    @Column(name = "name_rank")
    public String getNameRank() {
        return nameRank;
    }

    public void setNameRank(String nameRank) {
        this.nameRank = nameRank;
    }

    @Basic
    @Column(name = "value_rank")
    public String getValueRank() {
        return valueRank;
    }

    public void setValueRank(String valueRank) {
        this.valueRank = valueRank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RankEntity that = (RankEntity) o;

        if (idRank != that.idRank) return false;
        if (nameRank != null ? !nameRank.equals(that.nameRank) : that.nameRank != null) return false;
        if (valueRank != null ? !valueRank.equals(that.valueRank) : that.valueRank != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idRank;
        result = 31 * result + (nameRank != null ? nameRank.hashCode() : 0);
        result = 31 * result + (valueRank != null ? valueRank.hashCode() : 0);
        return result;
    }
}
