package mvc.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "question_list", schema = "MEDLEY_BD_OSS3", catalog = "")
@IdClass(QuestionListEntityPK.class)
public class QuestionListEntity {
    private int idList;
    private int idQuestion;

    @Id
    @Column(name = "id_list")
    public int getIdList() {
        return idList;
    }

    public void setIdList(int idList) {
        this.idList = idList;
    }

    @Id
    @Column(name = "id_question")
    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuestionListEntity that = (QuestionListEntity) o;

        if (idList != that.idList) return false;
        if (idQuestion != that.idQuestion) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idList;
        result = 31 * result + idQuestion;
        return result;
    }
}
