package mvc.models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class TutorialTechnologyEntityPK implements Serializable {
    private int idTutorial;
    private int idTechnology;

    @Column(name = "id_tutorial")
    @Id
    public int getIdTutorial() {
        return idTutorial;
    }

    public void setIdTutorial(int idTutorial) {
        this.idTutorial = idTutorial;
    }

    @Column(name = "id_technology")
    @Id
    public int getIdTechnology() {
        return idTechnology;
    }

    public void setIdTechnology(int idTechnology) {
        this.idTechnology = idTechnology;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TutorialTechnologyEntityPK that = (TutorialTechnologyEntityPK) o;

        if (idTutorial != that.idTutorial) return false;
        if (idTechnology != that.idTechnology) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTutorial;
        result = 31 * result + idTechnology;
        return result;
    }
}
