package mvc.models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class QuestionTechnologyEntityPK implements Serializable {
    private int idTechnology;
    private int idQuestion;

    @Column(name = "id_technology")
    @Id
    public int getIdTechnology() {
        return idTechnology;
    }

    public void setIdTechnology(int idTechnology) {
        this.idTechnology = idTechnology;
    }

    @Column(name = "id_question")
    @Id
    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuestionTechnologyEntityPK that = (QuestionTechnologyEntityPK) o;

        if (idTechnology != that.idTechnology) return false;
        if (idQuestion != that.idQuestion) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTechnology;
        result = 31 * result + idQuestion;
        return result;
    }
}
