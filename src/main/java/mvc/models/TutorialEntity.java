package mvc.models;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "tutorial", schema = "MEDLEY_BD_OSS3", catalog = "")
public class TutorialEntity {
    private int idTutorial;
    private String titleTuto;
    private String contentTuto;
    private String pictureTuto;
    private Date dateTuto;
    private Integer idUser;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_tutorial")
    public int getIdTutorial() {
        return idTutorial;
    }

    public void setIdTutorial(int idTutorial) {
        this.idTutorial = idTutorial;
    }

    @Basic
    @Column(name = "title_tuto")
    public String getTitleTuto() {
        return titleTuto;
    }

    public void setTitleTuto(String titleTuto) {
        this.titleTuto = titleTuto;
    }


    @Column(name = "content_tuto", columnDefinition = "TEXT")
    public String getContentTuto() {
        return contentTuto;
    }

    public void setContentTuto(String contentTuto) {
        this.contentTuto = contentTuto;
    }

    @Basic
    @Column(name = "picture_tuto")
    public String getPictureTuto() {
        return pictureTuto;
    }

    public void setPictureTuto(String pictureTuto) {
        this.pictureTuto = pictureTuto;
    }

    @Basic
    @Column(name = "date_tuto")
    public Date getDateTuto() {
        return dateTuto;
    }

    public void setDateTuto(Date dateTuto) {
        this.dateTuto = dateTuto;
    }

    @Basic
    @Column(name = "id_user")
    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TutorialEntity that = (TutorialEntity) o;

        if (idTutorial != that.idTutorial) return false;
        if (titleTuto != null ? !titleTuto.equals(that.titleTuto) : that.titleTuto != null) return false;
        if (contentTuto != null ? !contentTuto.equals(that.contentTuto) : that.contentTuto != null) return false;
        if (pictureTuto != null ? !pictureTuto.equals(that.pictureTuto) : that.pictureTuto != null) return false;
        if (dateTuto != null ? !dateTuto.equals(that.dateTuto) : that.dateTuto != null) return false;
        if (idUser != null ? !idUser.equals(that.idUser) : that.idUser != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTutorial;
        result = 31 * result + (titleTuto != null ? titleTuto.hashCode() : 0);
        result = 31 * result + (contentTuto != null ? contentTuto.hashCode() : 0);
        result = 31 * result + (pictureTuto != null ? pictureTuto.hashCode() : 0);
        result = 31 * result + (dateTuto != null ? dateTuto.hashCode() : 0);
        result = 31 * result + (idUser != null ? idUser.hashCode() : 0);
        return result;
    }
}
