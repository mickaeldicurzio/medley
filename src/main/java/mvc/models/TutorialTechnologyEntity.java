package mvc.models;

import javax.persistence.*;

@Entity
@Table(name = "tutorial_technology", schema = "MEDLEY_BD_OSS3", catalog = "")
@IdClass(TutorialTechnologyEntityPK.class)
public class TutorialTechnologyEntity {
    private int idTutorial;
    private int idTechnology;

    @Id
    @Column(name = "id_tutorial")
    public int getIdTutorial() {
        return idTutorial;
    }

    public void setIdTutorial(int idTutorial) {
        this.idTutorial = idTutorial;
    }

    @Id
    @Column(name = "id_technology")
    public int getIdTechnology() {
        return idTechnology;
    }

    public void setIdTechnology(int idTechnology) {
        this.idTechnology = idTechnology;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TutorialTechnologyEntity that = (TutorialTechnologyEntity) o;

        if (idTutorial != that.idTutorial) return false;
        if (idTechnology != that.idTechnology) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTutorial;
        result = 31 * result + idTechnology;
        return result;
    }
}
