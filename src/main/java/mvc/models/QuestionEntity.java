package mvc.models;


import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "question", schema = "MEDLEY_BD_OSS3", catalog = "")
public class QuestionEntity {
    private int idQuestion;
    private String title;
    private String content;
    private Integer idUser;
    private Timestamp dateQuestion;
    private Integer idDifficulty;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_question")
    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    @Basic
    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "id_user")
    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    @Basic
    @Column(name = "date_question")
    public Timestamp getDateQuestion() {
        return dateQuestion;
    }

    public void setDateQuestion(Timestamp dateQuestion) {
        this.dateQuestion = dateQuestion;
    }

    @Basic
    @Column(name = "id_difficulty")
    public Integer getIdDifficulty() {
        return idDifficulty;
    }

    public void setIdDifficulty(Integer idDifficulty) {
        this.idDifficulty = idDifficulty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuestionEntity that = (QuestionEntity) o;

        if (idQuestion != that.idQuestion) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (content != null ? !content.equals(that.content) : that.content != null) return false;
        if (idUser != null ? !idUser.equals(that.idUser) : that.idUser != null) return false;
        if (dateQuestion != null ? !dateQuestion.equals(that.dateQuestion) : that.dateQuestion != null) return false;
        if (idDifficulty != null ? !idDifficulty.equals(that.idDifficulty) : that.idDifficulty != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idQuestion;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (idUser != null ? idUser.hashCode() : 0);
        result = 31 * result + (dateQuestion != null ? dateQuestion.hashCode() : 0);
        result = 31 * result + (idDifficulty != null ? idDifficulty.hashCode() : 0);
        return result;
    }
}
