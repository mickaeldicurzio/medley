package mvc.models;

import javax.persistence.*;

@Entity
@Table(name = "contributor", schema = "MEDLEY_BD_OSS3", catalog = "")
public class ContributorEntity {
    private String profilPicture;
    private int idUser;
    private Integer idRank;

    @Basic
    @Column(name = "profil_picture")
    public String getProfilPicture() {
        return profilPicture;
    }

    public void setProfilPicture(String profilPicture) {
        this.profilPicture = profilPicture;
    }

    @Id
    @Column(name = "id_user")
    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Basic
    @Column(name = "id_rank")
    public Integer getIdRank() {
        return idRank;
    }

    public void setIdRank(Integer idRank) {
        this.idRank = idRank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContributorEntity that = (ContributorEntity) o;

        if (idUser != that.idUser) return false;
        if (profilPicture != null ? !profilPicture.equals(that.profilPicture) : that.profilPicture != null)
            return false;
        if (idRank != null ? !idRank.equals(that.idRank) : that.idRank != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = profilPicture != null ? profilPicture.hashCode() : 0;
        result = 31 * result + idUser;
        result = 31 * result + (idRank != null ? idRank.hashCode() : 0);
        return result;
    }
}
