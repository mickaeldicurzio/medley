package mvc.controller;

import mvc.actions.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;


@Path("/ws")
public class SuperController extends HttpServlet {

	private static final long serialVersionUID = 1L;
	public static final URI BASE_URI = getBaseURI();

	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8080/rest/").build();
	}

    //
    // QUESTION REQUEST RESEIVER
	//
	
	@GET()
    @Path("/question")
    @Produces(MediaType.APPLICATION_JSON)
    public String getQuestion() {
        QuestionAction actionQuestion = new QuestionAction();
        return actionQuestion.getQuestions();
    }

    @GET()
    @Path("/question/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getQuestionById(@PathParam("id") Integer id){
	    QuestionAction actionQuestion = new QuestionAction();
	    return actionQuestion.getQuestionById(id);
    }
    
    @GET()
    @Path("/questionwithdifficulty")
    @Produces(MediaType.APPLICATION_JSON)
    public String getQuestionWithDifficulty(){
    	QuestionAction actionQuestion = new QuestionAction();
	    return actionQuestion.getQuestionsWithDifficulty();
    }
    
    @GET()
    @Path("/questionwithoutdifficulty")
    @Produces(MediaType.APPLICATION_JSON)
    public String getQuestionWithoutDifficulty(){
    	QuestionAction actionQuestion = new QuestionAction();
	    return actionQuestion.getQuestionsWithoutDifficulty();
    }
    //
    // TUTORIAL REQUEST RECEIVER
    //

    @GET()
    @Path("/tutorial")
    @Produces(MediaType.APPLICATION_JSON)
    public String getTutorial() {
        TutorialAction tutorialAction = new TutorialAction();
        return tutorialAction.getTutorials();
    }

    @GET()
    @Path("/tutorial/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getTutorialById(@PathParam("id") Integer id){
        TutorialAction tutorialAction = new TutorialAction();
        return tutorialAction.getTutorialById(id);
    }

    @POST()
    @Path("/tutorial/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Context
    public Response createTutorial(String request) {
        TutorialAction tutorialAction = new TutorialAction();
        tutorialAction.createTutorial(request);

        Response response = Response.status(200).build();
        return response;
    }

    @DELETE()
    @Path("/tutorial/delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteTutorialById(@PathParam("id") Integer id) {
        TutorialAction tutorialAction = new TutorialAction();
        tutorialAction.deleteTutorialById(id);

        Response response = Response.status(200).build();
        return response;
    }


    //
    // TECHNOLOGY REQUEST RESEIVER
	//
    
	@GET()
    @Path("/technology")
    @Produces(MediaType.APPLICATION_JSON)
    public String getTechnos() {
		TechnologyAction getTechnoAction = new TechnologyAction();
        return getTechnoAction.getTechnos();
    }
	
    @GET()
    @Path("/technology/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getTechnoById(@PathParam("id") Integer id){
    	TechnologyAction technoPerCat = new TechnologyAction();
	    return technoPerCat.getTechnoById(id);
    }
    
    @GET()
    @Path("/categorybytechno/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getCategorieByTechno(@PathParam("id") Integer id){
    	TechnologyAction CategoryPerTechno = new TechnologyAction();
	    return CategoryPerTechno.getCategoryByTechno(id);
    }
    
    @POST()
    @Path("/technology/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Context
    public Response createTechno(String request) {
    	TechnologyAction createTechno = new TechnologyAction();
    	createTechno.createTechno(request);

        Response response = Response.status(200).build();
        return response;
    }

    @DELETE()
    @Path("/technology/delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteTechnoById(@PathParam("id") Integer id) {
    	TechnologyAction deleteTechno = new TechnologyAction();
    	deleteTechno.deleteTechnoById(id);

        Response response = Response.status(200).build();
        return response;
    }

    @PUT()
    @Path("/technology/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateTechnoById(@PathParam("id") Integer id,String request) {
    	TechnologyAction updateTechno = new TechnologyAction();
    	updateTechno.updateTechno(request, id);

        Response response = Response.status(200).build();
        return response;
    }
    
    //
    // CATEGORY REQUEST RESEIVER
	//
    
	@GET()
    @Path("/category")
    @Produces(MediaType.APPLICATION_JSON)
    public String getCategorie() {
		CategoryAction getCategorieAction = new CategoryAction();
        return getCategorieAction.getCategory();
    }

    @GET()
    @Path("/category/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getCategorieById(@PathParam("id") Integer id){
    	CategoryAction technoPerCat = new CategoryAction();
	    return technoPerCat.getCategoryById(id);
    }
    
    @GET()
    @Path("/technobycategory/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getTechnoByCategorie(@PathParam("id") Integer id){
    	CategoryAction technoPerCat = new CategoryAction();
	    return technoPerCat.getTechnoByCategory(id);
    }
    
    @POST()
    @Path("/category/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Context
    public Response createCategory(String request) {
        CategoryAction actionUser = new CategoryAction();
        actionUser.createCategory(request);

        Response response = Response.status(200).build();
        return response;
    }

    @DELETE()
    @Path("/category/delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCategoryById(@PathParam("id") Integer id) {
    	CategoryAction deleteCat = new CategoryAction();
    	deleteCat.deleteCategoryById(id);

        Response response = Response.status(200).build();
        return response;
    }

    @PUT()
    @Path("/category/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCategoryById(@PathParam("id") Integer id,String request) {
    	CategoryAction updateCat = new CategoryAction();
    	updateCat.updateCategory(request, id);

        Response response = Response.status(200).build();
        return response;
    }
    
    //
    // USER REQUEST RESEIVER
    //
    
    @GET()
    @Path("/user/login")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getConnectionToken(@Context HttpServletRequest request) {
        
    	ConnectionAction actionUser = new ConnectionAction();    	
    	Map<String, String> infos = getHeadersInfo(request);
        String userName = "";
        String userPwd = "";
        Response response = Response.status(1).build();
        
        for (Map.Entry<String, String> entry : infos.entrySet())
        {
        	if((entry.getKey()).equals("username")) {
        		userName = entry.getValue();
        	}else if((entry.getKey()).equals("password")) {
        		userPwd = entry.getValue();
        	}
        }
        		
        System.out.println(userName);
        System.out.println(userPwd);
        
        String Json = actionUser.connectionUser(userName, userPwd);
        
        if(Json == "false") {
            response = Response.status(401).build();
        } else {
        	response = Response.status(200).build();	
        	System.out.println("SA MARCHE LAAAAAAAAAAAAA");
        	System.out.println(Json);
        }
        return response;
    }
    
    
    @GET()
    @Path("/user")
    @Produces(MediaType.APPLICATION_JSON)
    public String getUsers() {
        UserAction actionUser = new UserAction();
        return actionUser.getUsers();
    }

    @GET()
    @Path("/user/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getUsersById(@PathParam("id") Integer id) {
        UserAction actionUser = new UserAction();
        return actionUser.getUserById(id);
    }
    
    @GET()
    @Path("/userlist/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getListByUser(@PathParam("id") Integer id) {
        UserAction actionUser = new UserAction();
        return actionUser.getListByUser(id);
    }

    @POST()
    @Path("/user/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Context
    public Response createUser(String request) {
        UserAction actionUser = new UserAction();
        actionUser.createUser(request);

        Response response = Response.status(200).build();
        return response;
    }

    @DELETE()
    @Path("/user/delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteUserById(@PathParam("id") Integer id) {
        UserAction userPerCat = new UserAction();
        userPerCat.deleteUserById(id);

        Response response = Response.status(200).build();
        return response;
    }

    @PUT()
    @Path("/user/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUserById(@PathParam("id") Integer id,String request) {
        UserAction actionUser = new UserAction();
        actionUser.updateUser(request, id);

        Response response = Response.status(200).build();
        return response;
    }
    
    //
    // LIST REQUEST RESEIVER
	//
    
	@GET()
    @Path("/list")
    @Produces(MediaType.APPLICATION_JSON)
    public String getList() {
		ListAction getListAction = new ListAction();
        return getListAction.getList();
    }

    @GET()
    @Path("/list/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getListById(@PathParam("id") Integer id){
    	ListAction listList = new ListAction();
	    return listList.getListById(id);
    }
    
    @GET()
    @Path("/questionbylist/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getQuestionByList(@PathParam("id") Integer id){
    	ListAction questionByList = new ListAction();
	    return questionByList.getQuestionByList(id);
    }
    
    @POST()
    @Path("/list/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Context
    public Response createList(String request) {
    	ListAction createList = new ListAction();
    	createList.createList(request);

        Response response = Response.status(200).build();
        return response;
    }

    @DELETE()
    @Path("/list/delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteListById(@PathParam("id") Integer id) {
    	ListAction deleteList = new ListAction();
    	deleteList.deleteListById(id);

        Response response = Response.status(200).build();
        return response;
    }

    @PUT()
    @Path("/list/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateListById(@PathParam("id") Integer id,String request) {
    	ListAction updateList = new ListAction();
    	updateList.updateList(request, id);

        Response response = Response.status(200).build();
        return response;
    }
    
    //UTILITILS CLASS
    
    private Map<String, String> getHeadersInfo(HttpServletRequest request) {

        Map<String, String> map = new HashMap<String, String>();

        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }

        return map;
    }
    
}

