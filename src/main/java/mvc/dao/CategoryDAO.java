package mvc.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

import mvc.models.CategoryEntity;
import mvc.utils.ConnectionDB;

public class CategoryDAO {

    ConnectionDB connectionDB = ConnectionDB.getInstance();
    
    /**
     * Method DAO using connection to execute a query to get all the category
     * @return categoryEntityListF
     */
    public List<CategoryEntity> getCategory(){
    	
        List<CategoryEntity> categoryEntityList = new ArrayList<CategoryEntity>();
        Session entityManager = connectionDB.openConnection();

        try {
            entityManager.beginTransaction();

            List<CategoryEntity> listCategory = entityManager.createQuery("FROM CategoryEntity").getResultList();
            
            for (Iterator<CategoryEntity> iterator = listCategory.iterator(); iterator.hasNext(); ) {
            	CategoryEntity category = (CategoryEntity) iterator.next();
            	categoryEntityList.add(category);
            }
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return categoryEntityList;
    }
    
    /**
     * Method DAO using connection to execute a query to get a category by id
     * @param id
     * @return
     */
    public CategoryEntity getCategoryById(Integer id){
    	
    	CategoryEntity categoryEntity = new CategoryEntity();
        Session entityManager = connectionDB.openConnection();

        try {
            entityManager.beginTransaction();

            categoryEntity = entityManager.find(CategoryEntity.class, id);
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return categoryEntity;
    }
    
    /**
     * Method DAO using connection to execute a query to get all the technology associated with a category
     * @param id
     * @return
     */
    public List<Object> getTechnoByCategory(int id){
    	
        List<Object> technoEntityList = new ArrayList<Object>();
        Session entityManager = connectionDB.openConnection();

        try {
        	entityManager.beginTransaction();

            Query<Object> query = entityManager.createQuery("SELECT t.idTechnology, t.nameTechnology, t.descTechnology FROM CategoryTechnologyEntity c, TechnologyEntity t WHERE c.idTechnology = t.idTechnology AND c.idCategory = :idCategory");
            query.setParameter("idCategory", id);
            List<Object> listTechno = query.getResultList();
            for (Iterator<Object> iterator = listTechno.iterator(); iterator.hasNext(); ) {
            	Object techno = iterator.next();
            	
            	technoEntityList.add(techno);
            }
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return technoEntityList;
    }
    
    /**
     * Method DAO using connection to execute a query to create a category
     * @param myCategory
     */
    public void createCategory(CategoryEntity myCategory) {
    	
        Session entityManager = connectionDB.openConnection();

        try {
        	entityManager.beginTransaction();
            entityManager.persist(myCategory);
            entityManager.getTransaction().commit();
        }catch(HibernateException e) {
        	
        }finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
    }
    
    /**
     * Method DAO using connection to execute a query to delete a category
     * @param id
     * @return
     */
    public CategoryEntity deleteCategory(int id) {
    	
        Session entityManager = connectionDB.openConnection();
        CategoryEntity category = entityManager.find(CategoryEntity.class, id);
        
        try {
            entityManager.beginTransaction();
            entityManager.remove(category);
            entityManager.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return category;
    }
    
    /**
     * Method DAO using connection to execute a query to update a category
     * @param myCategory
     * @return
     */
    public CategoryEntity updateCategory(CategoryEntity myCategory) {
    	
        Session entityManager = connectionDB.openConnection();
        CategoryEntity category = entityManager.find(CategoryEntity.class, myCategory.getIdCategory());
        
        try {
            entityManager.beginTransaction();
            
            Query<Object> query = entityManager.createQuery("UPDATE CategoryEntity Set nameCategory = :newNameCategory, descCategory = :newDescCategory WHERE idCategory = :idCategory");
            query.setParameter("newNameCategory", myCategory.getNameCategory());
            query.setParameter("newDescCategory", myCategory.getDescCategory());
            query.setParameter("idCategory", myCategory.getIdCategory());
            
            query.executeUpdate();
            entityManager.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return category;
    }
	
}
