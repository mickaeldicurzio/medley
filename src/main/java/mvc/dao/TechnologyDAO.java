package mvc.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

import mvc.models.TechnologyEntity;
import mvc.utils.ConnectionDB;

public class TechnologyDAO {

    ConnectionDB connectionDB = ConnectionDB.getInstance();
        
    /**
     * Method DAO using connection to execute a query to get a technology by id
     * @return
     */
    public List<TechnologyEntity> getTechnos(){
    	
        List<TechnologyEntity> technoEntityList = new ArrayList<TechnologyEntity>();
        Session entityManager = connectionDB.openConnection();

        try {
            entityManager.beginTransaction();

            List<TechnologyEntity> listTechno = entityManager.createQuery("FROM TechnologyEntity ").getResultList();
            for (Iterator<TechnologyEntity> iterator = listTechno.iterator(); iterator.hasNext(); ) {
            	TechnologyEntity techno = iterator.next();
            	technoEntityList.add(techno);
            }
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return technoEntityList;
    }
    
    /**
     * Method DAO using connection to execute a query to get all the technology by id
     * @param id
     * @return
     */
    public TechnologyEntity getTechnoById(Integer id){
    	
    	TechnologyEntity technoEntity = new TechnologyEntity();
        Session entityManager = connectionDB.openConnection();

        try {
            entityManager.beginTransaction();

            technoEntity = entityManager.find(TechnologyEntity.class, id);
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return technoEntity;
    }
    
    /**
     * Method DAO using connection to execute a query to get all the category associated with a technology
     * @param id
     * @return
     */
    public List<Object> getCategoryByTechno(int id){
    	
        List<Object> categoryEntityList = new ArrayList<Object>();
        Session entityManager = connectionDB.openConnection();

        try {
        	entityManager.beginTransaction();

            Query<Object> query = entityManager.createQuery("SELECT t.idCategory, t.nameCategory, t.descCategory FROM CategoryTechnologyEntity c, CategoryEntity t WHERE c.idCategory = t.idCategory AND c.idCategory = :idtechnology");
            query.setParameter("idtechnology", id);
            List<Object> listCategory = query.getResultList();
            for (Iterator<Object> iterator = listCategory.iterator(); iterator.hasNext(); ) {
            	Object techno = iterator.next();
            	
            	categoryEntityList.add(techno);
            }
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return categoryEntityList;
    }
    
    /**
     * Method DAO using connection to execute a query to create a technology
     * @param myTechnology
     */
    public void createTechnology(TechnologyEntity myTechnology) {
    	
        Session entityManager = connectionDB.openConnection();

        try {
        	entityManager.beginTransaction();
            entityManager.persist(myTechnology);
            entityManager.getTransaction().commit();
        }catch(HibernateException e) {
        	
        }finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
    }
    
    /**
     * Method DAO using connection to execute a query to delete a technology
     * @param id
     * @return
     */
    public TechnologyEntity deleteCategory(int id) {
    	
        Session entityManager = connectionDB.openConnection();
        TechnologyEntity category = entityManager.find(TechnologyEntity.class, id);
        
        try {
            entityManager.beginTransaction();
            entityManager.remove(category);
            entityManager.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return category;
    }
    
    /**
     * Method DAO using connection to execute a query to update a technology
     * @param myTechnology
     * @return
     */
    public TechnologyEntity updateCategory(TechnologyEntity myTechnology) {
    	
        Session entityManager = connectionDB.openConnection();
        TechnologyEntity category = entityManager.find(TechnologyEntity.class, myTechnology.getIdTechnology());
        
        try {
            entityManager.beginTransaction();
            
            Query<Object> query = entityManager.createQuery("UPDATE TechnologyEntity Set nameTechnology = :newNameTechnology, descTechnology = :newDescTechnology WHERE idTechnology = :idTechnology");
            query.setParameter("newNameTechnology", myTechnology.getNameTechnology());
            query.setParameter("newDescTechnology", myTechnology.getDescTechnology());
            query.setParameter("idTechnology", myTechnology.getIdTechnology());
            
            query.executeUpdate();
            entityManager.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return category;
    }
	
}
