package mvc.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import mvc.models.DifficultyEntity;
import mvc.utils.ConnectionDB;

public class DifficultyDAO {

    ConnectionDB connectionDB = ConnectionDB.getInstance();

    /**
     * Method DAO using connection to execute a query to get all the difficulty
     * @return categoryEntityListF
     */
    public List<DifficultyEntity> getDifficulty(){
    	
        List<DifficultyEntity> difficultyEntityList = new ArrayList<DifficultyEntity>();
        Session entityManager = connectionDB.openConnection();

        try {
            entityManager.beginTransaction();

            List<DifficultyEntity> listDifficulty = entityManager.createQuery("FROM DifficultyEntity").getResultList();
            
            for (Iterator<DifficultyEntity> iterator = listDifficulty.iterator(); iterator.hasNext(); ) {
            	DifficultyEntity difficulty = (DifficultyEntity) iterator.next();
            	difficultyEntityList.add(difficulty);
            }
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return difficultyEntityList;
    }
    
    /**
     * Method DAO using connection to execute a query to get a difficulty by id
     * @param id
     * @return
     */
    public DifficultyEntity getDifficultyById(Integer id){
    	
    	DifficultyEntity difficultyEntity = new DifficultyEntity();
        Session entityManager = connectionDB.openConnection();

        try {
            entityManager.beginTransaction();

            difficultyEntity = entityManager.find(DifficultyEntity.class, id);
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return difficultyEntity;
    }
    
}
