package mvc.dao;

import mvc.models.UsersEntity;
import mvc.utils.ConnectionDB;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class UserDAO {
	
    ConnectionDB connectionDB = ConnectionDB.getInstance();
        
    /**
     * Method DAO using connection to execute a query to get all the users
     * @return UsersEntityList
     */
    public List<UsersEntity> getUsers(){
    	
        List<UsersEntity> UsersEntityList = new ArrayList<UsersEntity>();
        Session entityManager = connectionDB.openConnection();

        try {
            entityManager.beginTransaction();

            List<UsersEntity> listUser = entityManager.createQuery("FROM UsersEntity").getResultList();
            
            for (Iterator<UsersEntity> iterator = listUser.iterator(); iterator.hasNext(); ) {
                UsersEntity user = (UsersEntity) iterator.next();
                UsersEntityList.add(user);
            }
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return UsersEntityList;
    }
    
    /**
     * Method DAO using connection to execute a query to get an user by id
     * @param id
     * @return UsersEntity
     */
    public UsersEntity getUserById(Integer id){
    	
        UsersEntity UsersEntity = new UsersEntity();
        Session entityManager = connectionDB.openConnection();

        try {
            entityManager.beginTransaction();

            UsersEntity = entityManager.find(UsersEntity.class, id);
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return UsersEntity;
    }
    
    /**
     * Method DAO using connection to execute a query to get an user pwd by his name
     * @param login
     * @return UsersEntity
     */
    public UsersEntity getPwdByUserName(String loginUser, String password){
   
        UsersEntity userEntity = new UsersEntity();
        List<Object> userEntityList = new ArrayList<Object>();
        Session entityManager = connectionDB.openConnection();

        try {
            entityManager.beginTransaction();

            Query<Object> query = entityManager.createQuery("FROM UsersEntity WHERE login = :Ulogin");
            query.setParameter("Ulogin", loginUser);
            List<Object> users = query.getResultList();
            
            for (Iterator<Object> iterator = users.iterator(); iterator.hasNext(); ) {
            	Object list = iterator.next();
            	userEntityList.add(list);
            }
            
            for (Iterator<Object> iterator2 = userEntityList.iterator(); iterator2.hasNext(); ) {
            	Object list = iterator2.next();
                UsersEntity user = new UsersEntity();

                user = (UsersEntity) list;
            	if(user.getPassword().equals(password)) {
            		userEntity = user;
            	}
            }
            
            System.out.println(userEntity);
            
        }catch (HibernateException e) {
            e.printStackTrace();
            
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return userEntity;
    }
    
    
    /**
     * Method DAO using connection to execute a query to get all the list associated with an user
     * @param id
     * @return
     */
    public List<Object> getListByUser(int id){
    	
        List<Object> listEntityList = new ArrayList<Object>();
        Session entityManager = connectionDB.openConnection();

        try {
        	entityManager.beginTransaction();

            Query<Object> query = entityManager.createQuery("FROM ListEntity WHERE idUser = :idUser");
            query.setParameter("idUser", id);
            List<Object> listList = query.getResultList();
            for (Iterator<Object> iterator = listList.iterator(); iterator.hasNext(); ) {
            	Object list = iterator.next();
            	
            	listEntityList.add(list);
            }
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return listEntityList;
    }

    /**
     * Method DAO using connection to execute a query who create an user
     * @param myUser
     */
    public void createUser(UsersEntity myUser) {
    	
        Session entityManager = connectionDB.openConnection();

        try {
        	entityManager.beginTransaction();
            entityManager.persist(myUser);
            entityManager.getTransaction().commit();
        }catch(HibernateException e) {
        	
        }finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
    }

    /**
     * Method DAO using connection to execute a query who delete an user
     * @param id
     * @return
     */
    public UsersEntity deleteUser(int id) {
    	
        Session entityManager = connectionDB.openConnection();
        UsersEntity user = entityManager.find(UsersEntity.class, id);
        
        try {
            entityManager.beginTransaction();
            entityManager.remove(user);
            entityManager.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return user;
    }

    /**
     * Method DAO using connection to execute a query who update an user
     * @param myUser
     * @return
     */
    public UsersEntity updateUser(UsersEntity myUser) {
    	
        Session entityManager = connectionDB.openConnection();
        UsersEntity user = entityManager.find(UsersEntity.class, myUser.getIdUser());
        
        try {
            entityManager.beginTransaction();
            
            Query<Object> query = entityManager.createQuery("UPDATE UsersEntity set login = :newLogin, password=:newPassword, firstName=:newFirstName, lastName=:newLastName, email=:newEmail, birthdayDate=:newBirthdayDate where idUser=:idUser");
            query.setParameter("newLogin", myUser.getLogin());
            query.setParameter("newPassword", myUser.getPassword());
            query.setParameter("newFirstName", myUser.getFirstName());
            query.setParameter("newLastName", myUser.getLastName());
            query.setParameter("newEmail", myUser.getEmail());
            query.setParameter("newBirthdayDate", myUser.getBirthdayDate());
            query.setParameter("idUser", myUser.getIdUser());
            
            query.executeUpdate();
            entityManager.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return user;
    }
}
