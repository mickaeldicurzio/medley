package mvc.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

import mvc.models.ListEntity;
import mvc.utils.ConnectionDB;

public class ListDAO {
	
    ConnectionDB connectionDB = ConnectionDB.getInstance();
	
    /**
     * Method DAO using connection to execute a query to get all the list
     * @return categoryEntityListF
     */
    public List<ListEntity> getList(){
    	
        List<ListEntity> ListEntityList = new ArrayList<ListEntity>();
        Session entityManager = connectionDB.openConnection();

        try {
            entityManager.beginTransaction();

            List<ListEntity> listoflist = entityManager.createQuery("FROM ListEntity").getResultList();
            
            for (Iterator<ListEntity> iterator = listoflist.iterator(); iterator.hasNext(); ) {
            	ListEntity user = (ListEntity) iterator.next();
            	ListEntityList.add(user);
            }
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return ListEntityList;
    }
    
    /**
     * Method DAO using connection to execute a query to get a list by id
     * @param id
     * @return
     */
    public ListEntity getListById(Integer id){
    	
    	ListEntity listEntity = new ListEntity();
        Session entityManager = connectionDB.openConnection();

        try {
            entityManager.beginTransaction();

            listEntity = entityManager.find(ListEntity.class, id);
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return listEntity;
    }

    /**
     * Method DAO using connection to execute a query to get all the question associated with a list
     * @param id
     * @return
     */
    public List<Object> getQuestionByList(int id){
    	
        List<Object> questionEntityList = new ArrayList<Object>();
        Session entityManager = connectionDB.openConnection();

        try {
        	entityManager.beginTransaction();

            Query<Object> query = entityManager.createQuery("SELECT ae.idQuestion, ae.title, ae.content, ae.idUser, ae.idDifficulty FROM QuestionListEntity a, QuestionEntity ae WHERE a.idQuestion = ae.idQuestion AND a.idList = :idList");
            query.setParameter("idList", id);
            List<Object> listQuestion = query.getResultList();
            for (Iterator<Object> iterator = listQuestion.iterator(); iterator.hasNext(); ) {
            	Object question = iterator.next();
            	
            	questionEntityList.add(question);
            }
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return questionEntityList;
    }
    
    /**
     * Method DAO using connection to execute a query to create a list
     * @param myCategory
     */
    public void createList(ListEntity myList) {
    	
        Session entityManager = connectionDB.openConnection();

        try {
        	entityManager.beginTransaction();
            entityManager.persist(myList);
            entityManager.getTransaction().commit();
        }catch(HibernateException e) {
        	
        }finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
    }
    
    /**
     * Method DAO using connection to execute a query to delete a list
     * @param id
     * @return
     */
    public ListEntity deleteList(int id) {
    	
        Session entityManager = connectionDB.openConnection();
        ListEntity list = entityManager.find(ListEntity.class, id);
        
        try {
            entityManager.beginTransaction();
            entityManager.remove(list);
            entityManager.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return list;
    }
    
    /**
     * Method DAO using connection to execute a query to update a list
     * @param myCategory
     * @return
     */
    public ListEntity updateList(ListEntity myList) {
    	
        Session entityManager = connectionDB.openConnection();
        ListEntity list = entityManager.find(ListEntity.class, myList.getIdList());
        
        try {
            entityManager.beginTransaction();
            
            Query<Object> query = entityManager.createQuery("UPDATE ListEntity SET nameList = :newNameList, idUser = :newIdUser WHERE idList = :newIdList");
            query.setParameter("newNameList", myList.getNameList());
            query.setParameter("newIdUser", myList.getIdUser());
            query.setParameter("newIdList", myList.getIdList());
            
            query.executeUpdate();
            entityManager.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return list;
    }
    
}
