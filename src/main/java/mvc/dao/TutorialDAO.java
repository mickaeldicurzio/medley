package mvc.dao;

import mvc.models.CategoryEntity;
import mvc.models.TutorialCategoryEntity;
import mvc.models.TutorialEntity;
import mvc.models.TutorialTechnologyEntity;
import mvc.utils.ConnectionDB;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TutorialDAO {

    ConnectionDB connectionDB = ConnectionDB.getInstance();

    /**
     * Method DAO using connection to execute a query to get all the category
     * @return categoryEntityListF
     */
    public List<TutorialEntity> getTutorial(){

        List<TutorialEntity> tutorialEntityList = new ArrayList<TutorialEntity>();
        Session entityManager = connectionDB.openConnection();

        try {
            entityManager.beginTransaction();

            List<TutorialEntity> entityList = entityManager.createQuery("FROM TutorialEntity ").getResultList();

            for (Iterator<TutorialEntity> iterator = entityList.iterator(); iterator.hasNext(); ) {
                TutorialEntity category = (TutorialEntity) iterator.next();
                tutorialEntityList.add(category);
            }
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return tutorialEntityList;
    }

    public int countTutorial(){

        TutorialEntity tutorialEntity = new TutorialEntity();
        Session entityManager = connectionDB.openConnection();
        int nbTuto = 0;
        try{

            List<TutorialEntity> entityList = entityManager.createQuery("FROM TutorialEntity ").getResultList();
            nbTuto = entityList.size();
        }catch (Exception e){
            System.out.println(e);
        }
        return nbTuto;
    }
    /**
     * Method DAO using connection to execute a query to get a category by id
     * @param id
     * @return
     */
    public TutorialEntity getTutorialById(Integer id){

        TutorialEntity tutorialEntity = new TutorialEntity();
        Session entityManager = connectionDB.openConnection();

        try {
            entityManager.beginTransaction();

            tutorialEntity = entityManager.find(TutorialEntity.class, id);
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return tutorialEntity;
    }

    /**
     * Method DAO using connection to execute a query to get all the technology associated with a category
     * @param id
     * @return
     */
//    public List<Object> getTechnoByCategory(int id){
//
//        List<Object> tutorialEntityList = new ArrayList<Object>();
//        Session entityManager = connectionDB.openConnection();
//
//        try {
//            entityManager.beginTransaction();
//
//            Query<Object> query = entityManager.createQuery("SELECT t.idTechnology, t.nameTechnology, t.descTechnology FROM CategoryTechnologyEntity c, TechnologyEntity t WHERE c.idTechnology = t.idTechnology AND c.idCategory = :idCategory");
//            query.setParameter("idCategory", id);
//            List<Object> listTechno = query.getResultList();
//            for (Iterator<Object> iterator = listTechno.iterator(); iterator.hasNext(); ) {
//                Object techno = iterator.next();
//
//                tutorialEntityList.add(techno);
//            }
//        }catch (HibernateException e) {
//            e.printStackTrace();
//        } finally {
//            entityManager.close();
//            connectionDB.closeConnection();
//        }
//        return tutorialEntityList;
//    }

    /**
     * Method DAO using connection to execute a query to create a category
     * @param myTutorial
     */
    public void createTutorial(TutorialEntity myTutorial) {

        Session entityManager = connectionDB.openConnection();

        try {
            entityManager.beginTransaction();
            entityManager.persist(myTutorial);
            entityManager.getTransaction().commit();
        }catch(HibernateException e) {

        }finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
    }
    public void createTutorialTechnology(TutorialTechnologyEntity tutorialTechnologyEntity) {
        Session entityManager = connectionDB.openConnection();

        try {
            entityManager.beginTransaction();
            entityManager.persist(tutorialTechnologyEntity);
            entityManager.getTransaction().commit();
        }catch(HibernateException e) {

        }finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
    }
    public void createTutorialCategory(TutorialCategoryEntity tce) {
        Session entityManager = connectionDB.openConnection();

        try {
            entityManager.beginTransaction();
            entityManager.persist(tce);
            entityManager.getTransaction().commit();
        }catch(HibernateException e) {

        }finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
    }

    /**
     * Method DAO using connection to execute a query to delete a category
     * @param id
     * @return
     */
    public TutorialEntity deleteTutorial(int id) {

        Session entityManager = connectionDB.openConnection();
        TutorialEntity tutorialEntity = entityManager.find(TutorialEntity.class, id);

        try {
            entityManager.beginTransaction();
            entityManager.remove(tutorialEntity);
            entityManager.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return tutorialEntity;
    }

    /**
     * Method DAO using connection to execute a query to update a category
     * @param myTutorial
     * @return
     */
    public TutorialEntity updateTutorial(TutorialEntity myTutorial) {

        Session entityManager = connectionDB.openConnection();
        TutorialEntity tutorialEntity = entityManager.find(TutorialEntity.class, myTutorial.getIdTutorial());

        try {
            entityManager.beginTransaction();

            Query<Object> query = entityManager.createQuery("UPDATE TutorialEntity Set titleTuto = :newNameTutorial, contentTuto = :newDescTutorial, pictureTuto = :newPicture, dateTuto = :newDate WHERE idTutorial = :idTutorial");
            query.setParameter("newNameTutorial", myTutorial.getTitleTuto());
            query.setParameter("newDescTutorial", myTutorial.getContentTuto());
            query.setParameter("newPicture", myTutorial.getPictureTuto());
            query.setParameter("newDate", myTutorial.getDateTuto());
            query.setParameter("idTutorial", myTutorial.getIdTutorial());


            query.executeUpdate();
            entityManager.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return tutorialEntity;
    }


}
