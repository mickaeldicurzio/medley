package mvc.dao;

import mvc.models.QuestionEntity;
import mvc.utils.ConnectionDB;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class QuestionDAO {

    ConnectionDB connectionDB = ConnectionDB.getInstance();
    
    Session entityManager = connectionDB.openConnection();
    
    public List<QuestionEntity> getQuestions(){
        List<QuestionEntity> questionEntityList = new ArrayList<QuestionEntity>();
        try {
            entityManager.beginTransaction();

            List<QuestionEntity> listQuestion = entityManager.createQuery("FROM QuestionEntity").getResultList();
            for (Iterator<QuestionEntity> iterator = listQuestion.iterator(); iterator.hasNext(); ) {
            	QuestionEntity question = iterator.next();
            	questionEntityList.add(question);
            }
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return questionEntityList;
    }
    
    public QuestionEntity getQuestionById(Integer id){
    	QuestionEntity questionEntity = new QuestionEntity();
        try {
            entityManager.beginTransaction();

            questionEntity = entityManager.find(QuestionEntity.class, id);
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return questionEntity;
    }
    
    /**
     * Method DAO using connection to execute a query to get all question that don't have difficulty set
     * @return
     */
    public List<QuestionEntity> getQuestionsWithDifficulty(){
        List<QuestionEntity> questionEntityList = new ArrayList<QuestionEntity>();
        try {
            entityManager.beginTransaction();

            List<QuestionEntity> listQuestion = entityManager.createQuery("FROM QuestionEntity WHERE idDifficulty != null").getResultList();
            for (Iterator<QuestionEntity> iterator = listQuestion.iterator(); iterator.hasNext(); ) {
            	QuestionEntity question = iterator.next();
            	questionEntityList.add(question);
            }
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return questionEntityList;
    }
    
    /**
     * Method DAO using connection to execute a query to get all question that don't have difficulty set
     * @return
     */
    public List<QuestionEntity> getQuestionsWithoutDifficulty(){
        List<QuestionEntity> questionEntityList = new ArrayList<QuestionEntity>();
        try {
            entityManager.beginTransaction();

            List<QuestionEntity> listQuestion = entityManager.createQuery("FROM QuestionEntity WHERE idDifficulty = null").getResultList();
            for (Iterator<QuestionEntity> iterator = listQuestion.iterator(); iterator.hasNext(); ) {
            	QuestionEntity question = iterator.next();
            	questionEntityList.add(question);
            }
        }catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
            connectionDB.closeConnection();
        }
        return questionEntityList;
    }

}
