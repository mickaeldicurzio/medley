package mvc.utils;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class ConnectionDB {
    Session session;
    
    private ConnectionDB()
    {}
     
    private static ConnectionDB INSTANCE = null;
     
    public static ConnectionDB getInstance()
    {           
        if (INSTANCE == null)
        {   INSTANCE = new ConnectionDB(); 
        }
        return INSTANCE;
    }

    public Session openConnection() {
        this.session = HibernateUtil.getSession();
        return session;
    }

    public void closeConnection() {
        this.session = HibernateUtil.getSession();
        session.close();
    }

}
