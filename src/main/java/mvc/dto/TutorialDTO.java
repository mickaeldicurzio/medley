package mvc.dto;

import java.sql.Date;

public class TutorialDTO {

    private int idTutorial;
    private String titleTuto;
    private String contentTuto;
    private String pictureTuto;
    private Date dateTuto;
    private Integer idUser;
    private Integer idCategory;
    private Integer idTechnology;

    public TutorialDTO() {
    }

    public int getIdTutorial() {
        return idTutorial;
    }

    public void setIdTutorial(int idTutorial) {
        this.idTutorial = idTutorial;
    }

    public String getTitleTuto() {
        return titleTuto;
    }

    public void setTitleTuto(String titleTuto) {
        this.titleTuto = titleTuto;
    }

    public String getContentTuto() {
        return contentTuto;
    }

    public void setContentTuto(String contentTuto) {
        this.contentTuto = contentTuto;
    }

    public String getPictureTuto() {
        return pictureTuto;
    }

    public void setPictureTuto(String pictureTuto) {
        this.pictureTuto = pictureTuto;
    }

    public Date getDateTuto() {
        return dateTuto;
    }

    public void setDateTuto(Date dateTuto) {
        this.dateTuto = dateTuto;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Integer idCategory) {
        this.idCategory = idCategory;
    }

    public Integer getIdTechnology() {
        return idTechnology;
    }

    public void setIdTechnology(Integer idTechnology) {
        this.idTechnology = idTechnology;
    }


}
