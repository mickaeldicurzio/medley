package mvc.dto;

import java.sql.Date;
import java.util.UUID;

public class UserDTO {
    private int idUser;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private Date birthdayDate;
    private String token;

    public UserDTO(int idUser, String login, String password, String firstName, String lastName, String email, Date birthdayDate) {
        this.idUser = idUser;
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.birthdayDate = birthdayDate;
        this.token = UUID.randomUUID().toString();
    }
    
    public UserDTO() {
        this.token = UUID.randomUUID().toString();
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthdayDate() {
        return birthdayDate;
    }

    public void setBirthdayDate(Date birthdayDate) {
        this.birthdayDate = birthdayDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
