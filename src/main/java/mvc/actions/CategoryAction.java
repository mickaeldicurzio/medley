package mvc.actions;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import mvc.dao.CategoryDAO;
import mvc.models.CategoryEntity;

public class CategoryAction {

    CategoryDAO categoryDAO = new CategoryDAO();
    ObjectMapper objectMapper = new ObjectMapper();
    
    public String getCategory(){
        String myJsonString = "";
        try {
             myJsonString = objectMapper.writeValueAsString(categoryDAO.getCategory());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return myJsonString;
    }
    
    public String getCategoryById(Integer id){
    	String myJsonString = "";
    		try {
    			myJsonString = objectMapper.writeValueAsString(categoryDAO.getCategoryById(id));
    		} catch (JsonProcessingException e) {
    			e.printStackTrace();
    		}
    	return myJsonString;
    }
    
    public String getTechnoByCategory(int id){
        ObjectMapper objectMapper = new ObjectMapper();
        String myJsonString = "";
        try {
             myJsonString = objectMapper.writeValueAsString(categoryDAO.getTechnoByCategory(id));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return myJsonString;
    }
    
    public void createCategory(String myJsonUser) {
        CategoryEntity newCategory = new CategoryEntity();
        try {
        	newCategory = objectMapper.readValue(myJsonUser, CategoryEntity.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        categoryDAO.createCategory(newCategory);
    }
    
    public void deleteCategoryById(int id) {
    	if(this.getCategoryById(id) != null)
		{
		    categoryDAO.deleteCategory(id);
		}
    }
    
    public void updateCategory(String myJsonUser, Integer id) {
    	CategoryEntity category = new CategoryEntity();
        try {
            category = objectMapper.readValue(myJsonUser, CategoryEntity.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        category.setIdCategory(id);
        categoryDAO.updateCategory(category);
    }
	
}
