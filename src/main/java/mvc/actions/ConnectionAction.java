package mvc.actions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mvc.dao.UserDAO;
import mvc.dto.UserDTO;
import mvc.models.UsersEntity;


public class ConnectionAction {
	
    UserDAO userDAO = new UserDAO();
    UserDTO userDTO = new UserDTO();
	String myJsonString = "";
    
    ObjectMapper objectMapper = new ObjectMapper();
    
    public String connectionUser(String userName, String password){
        
    	UsersEntity entity = userDAO.getPwdByUserName(userName, password);
    	
    	if(password.equals(entity.getPassword())) {
    		
    		userDTO = userEntityToUserDTO(entity);
    		
          try {
        	  myJsonString = objectMapper.writeValueAsString(userDTO);
          } catch (JsonProcessingException e) {
        	  e.printStackTrace();
          }
    	}else{
    		
    		myJsonString = "false";
    	}
		return myJsonString;
    }
    
    private UserDTO userEntityToUserDTO(UsersEntity userDAO) {
    	
    	UserDTO user = new UserDTO();
    	
    	user.setIdUser(userDAO.getIdUser());
    	user.setLogin(userDAO.getLogin());
    	user.setPassword(userDAO.getPassword());
    	user.setFirstName(userDAO.getFirstName());
    	user.setLastName(userDAO.getLastName());
    	user.setEmail(userDAO.getEmail());
    	user.setBirthdayDate(userDAO.getBirthdayDate());

    	return user;
    }
}
