package mvc.actions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mvc.dao.QuestionDAO;

public class QuestionAction {
    QuestionDAO questionDAO = new QuestionDAO();
    ObjectMapper objectMapper = new ObjectMapper();

    public String getQuestions(){
        String myJsonString = "";
        try {
             myJsonString = objectMapper.writeValueAsString(questionDAO.getQuestions());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return myJsonString;
    }

    public String getQuestionById(Integer id){
        String myJsonString = "";
        try {
            myJsonString = objectMapper.writeValueAsString(questionDAO.getQuestionById(id));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return myJsonString;
    }
    
    public String getQuestionsWithDifficulty(){
        String myJsonString = "";
        try {
             myJsonString = objectMapper.writeValueAsString(questionDAO.getQuestionsWithDifficulty());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return myJsonString;
    }
    
    public String getQuestionsWithoutDifficulty(){
        String myJsonString = "";
        try {
             myJsonString = objectMapper.writeValueAsString(questionDAO.getQuestionsWithoutDifficulty());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return myJsonString;
    }
}
