package mvc.actions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mvc.dao.TutorialDAO;
import mvc.dto.TutorialDTO;
import mvc.models.CategoryEntity;
import mvc.models.TutorialCategoryEntity;
import mvc.models.TutorialEntity;
import mvc.models.TutorialTechnologyEntity;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TutorialAction {

    TutorialDAO tutorialDAO = new TutorialDAO();
    ObjectMapper objectMapper = new ObjectMapper();

    public String getTutorials(){
        String myJsonString = "";
        try {
            myJsonString = objectMapper.writeValueAsString(tutorialDAO.getTutorial());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return myJsonString;
    }

    public String getTutorialById(Integer id){
        String myJsonString = "";
        try {
            myJsonString = objectMapper.writeValueAsString(tutorialDAO.getTutorialById(id));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return myJsonString;
    }


    public String createTutorial(String myJsonUser) {
        try {
            TutorialDTO tutorialDTO = objectMapper.readValue(myJsonUser, TutorialDTO.class);
            Map<String, Object> myMap = tutorialDTOToTutorialEntity(tutorialDTO);
            tutorialDAO.createTutorial( (TutorialEntity) myMap.get("tutorialEntity"));
            tutorialDAO.createTutorialCategory( (TutorialCategoryEntity) myMap.get("tutorialCategoryEntity"));
            tutorialDAO.createTutorialTechnology( (TutorialTechnologyEntity) myMap.get("tutorialTechnologyEntity"));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return myJsonUser;
    }

    public void deleteTutorialById(int id) {
        if(this.getTutorialById(id) != null)
        {
            tutorialDAO.deleteTutorial(id);
        }
    }

    public void updateTutorial(String myJsonUser, Integer id) {
        TutorialEntity tutorialEntity = new TutorialEntity();
        try {
            tutorialEntity = objectMapper.readValue(myJsonUser, TutorialEntity.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        tutorialEntity.setIdUser(id);
        tutorialDAO.updateTutorial(tutorialEntity);
    }

    private Map<String, Object> tutorialDTOToTutorialEntity(TutorialDTO tutorialDTO)
    {
        Map<String, Object> myMap = new HashMap<>();
        int nbtuto = tutorialDAO.countTutorial() +1 ;
        TutorialEntity tutorialEntity = new TutorialEntity();
        TutorialCategoryEntity tutorialCategoryEntity = new TutorialCategoryEntity();
        TutorialTechnologyEntity tutorialTechnologyEntity = new TutorialTechnologyEntity();

        tutorialEntity.setTitleTuto(tutorialDTO.getTitleTuto());
        tutorialEntity.setContentTuto(tutorialDTO.getContentTuto());
        tutorialEntity.setPictureTuto(tutorialDTO.getPictureTuto());
        tutorialEntity.setDateTuto(tutorialDTO.getDateTuto());
        tutorialEntity.setIdUser(tutorialDTO.getIdUser());
        tutorialCategoryEntity.setIdTutorial(nbtuto);
        tutorialCategoryEntity.setIdCategory(tutorialDTO.getIdCategory());
        tutorialTechnologyEntity.setIdTutorial(nbtuto);
        tutorialTechnologyEntity.setIdTechnology(tutorialDTO.getIdTechnology());

        myMap.put("tutorialEntity", tutorialEntity);
        myMap.put("tutorialCategoryEntity", tutorialCategoryEntity);
        myMap.put("tutorialTechnologyEntity", tutorialTechnologyEntity);

        return myMap;
    }

}
