package mvc.actions;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import mvc.dao.TechnologyDAO;
import mvc.models.TechnologyEntity;

public class TechnologyAction {

    TechnologyDAO technoDAO = new TechnologyDAO();
    ObjectMapper objectMapper = new ObjectMapper();

    public String getTechnos(){
        String myJsonString = "";
        try {
             myJsonString = objectMapper.writeValueAsString(technoDAO.getTechnos());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return myJsonString;
    }
    
    public String getTechnoById(Integer id){
    	String myJsonString = "";
    		try {
    			myJsonString = objectMapper.writeValueAsString(technoDAO.getTechnoById(id));
    		} catch (JsonProcessingException e) {
    			e.printStackTrace();
    		}
    	return myJsonString;
    }
    
    public String getCategoryByTechno(int id){
        ObjectMapper objectMapper = new ObjectMapper();
        String myJsonString = "";
        try {
             myJsonString = objectMapper.writeValueAsString(technoDAO.getCategoryByTechno(id));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return myJsonString;
    }
    
    public void createTechno(String myJsonUser) {
        TechnologyEntity newTechno = new TechnologyEntity();
        try {
        	newTechno = objectMapper.readValue(myJsonUser, TechnologyEntity.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        technoDAO.createTechnology(newTechno);
    }
    
    public void deleteTechnoById(int id) {
    	if(this.getTechnoById(id) != null)
		{
		    technoDAO.deleteCategory(id);
		}
    }
    
    public void updateTechno(String myJsonUser, Integer id) {
    	TechnologyEntity technology = new TechnologyEntity();
        try {
            technology = objectMapper.readValue(myJsonUser, TechnologyEntity.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        technology.setIdTechnology(id);
        technoDAO.updateCategory(technology);
    }
    
}
